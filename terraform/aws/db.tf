module "amplicode-company-app_db" {
  source = "./db"

  name                                  = var.amplicode-company-app_db_name
  engine                                = var.amplicode-company-app_db_engine
  engine_version                        = var.amplicode-company-app_db_engine_version
  instance_class                        = var.amplicode-company-app_db_instance_class
  storage                               = var.amplicode-company-app_db_storage
  user                                  = var.amplicode-company-app_db_user
  password                              = var.amplicode-company-app_db_password
  random_password                       = var.amplicode-company-app_db_random_password
  vpc_id                                = module.vpc.id
  subnet_group_name                     = module.vpc.db_subnet_group_name
  multi_az                              = var.amplicode-company-app_db_multi_az
  source_security_group_id              = module.beanstalk.aws_security_group.id
}
