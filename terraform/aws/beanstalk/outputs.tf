output "public_url" {
  description = "Beanstalk Public URL"
  value       = join("", concat([var.cname_prefix != null ? "https://" : "http://"], aws_elastic_beanstalk_environment.env.*.cname))
}

output "prefix" {
  value = local.prefix
}

output "aws_security_group" {
  value = aws_security_group.instance
}

output "aws_elastic_beanstalk_environment" {
  value = aws_elastic_beanstalk_environment.env
}

output "aws_iam_role_ec2" {
  value = aws_iam_role.ec2
}