import { gql } from "@amplicode/gql";
import { Type } from "@amplicode/gql/graphql";
import { ResultOf } from "@graphql-typed-document-node/core";
import { useCallback } from "react";
import {
  Create,
  NumberInput,
  SimpleForm,
  TextInput,
  useCreate,
  useNotify,
  useRedirect,
} from "react-admin";
import { FieldValues, SubmitHandler } from "react-hook-form";
import { checkServerValidationErrors } from "../../../core/error/checkServerValidationError";
import { MAX_INT_VALUE, MIN_INT_VALUE } from "../../../core/format/constants";
import { formatNumber } from "../../../core/format/formatNumber";
import { parseNumber } from "../../../core/format/parseNumber";
import { EnumInput } from "../../../core/inputs/EnumInput";

const UPDATE_COMPANY = gql(`mutation UpdateCompany($input: CompanyInput!) {
  updateCompany(input: $input) {
    id
    name
    number
    type
  }
}`);

export const CompanyCreate = () => {
  const redirect = useRedirect();
  const notify = useNotify();
  const [create] = useCreate();

  const save: SubmitHandler<FieldValues> = useCallback(
    async (data: FieldValues) => {
      try {
        const params = { data, meta: { mutation: UPDATE_COMPANY } };
        const options = { returnPromise: true };

        await create("Company", params, options);

        notify("ra.notification.created", { messageArgs: { smart_count: 1 } });
        redirect("list", "Company");
      } catch (response: any) {
        console.log("create failed with error", response);
        return checkServerValidationErrors(response, notify);
      }
    },
    [create, notify, redirect]
  );

  return (
    <Create<ItemType> redirect="list">
      <SimpleForm onSubmit={save}>
        <TextInput source="name" name="name" />
        <NumberInput
          source="number"
          name="number"
          max={MAX_INT_VALUE}
          min={MIN_INT_VALUE}
          format={formatNumber}
          parse={parseNumber}
        />
        <EnumInput name="type" source="type" enumTypeName="Type" enum={Type} />
      </SimpleForm>
    </Create>
  );
};

const COMPANY_TYPE = gql(`query Company($id: ID!) {
  company(id: $id) {
    id
    name
    number
    type
  }
}`);

/**
 * Type of data object received when executing the query
 */
type QueryResultType = ResultOf<typeof COMPANY_TYPE>;
/**
 * Type of the item loaded by executing the query
 */
type ItemType = { id: string } & Exclude<QueryResultType["company"], undefined>;
