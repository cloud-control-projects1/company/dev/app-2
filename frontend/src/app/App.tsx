import { DevSupport } from "@react-buddy/ide-toolbox";
import { AdminContext, AdminUI, Loading, Resource } from "react-admin";
import { useAuthProvider } from "../authProvider/useAuthProvider";
import { getCompanyRecordRepresentation } from "../core/record-representation/getCompanyRecordRepresentation";
import { dataProvider } from "../dataProvider/graphqlDataProvider";
import { ComponentPreviews, useInitial } from "../dev";
import { i18nProvider } from "../i18nProvider";
import { AdminLayout } from "./AdminLayout";
import { CompanyCreate } from "./screens/company/CompanyCreate";
import { CompanyEdit } from "./screens/company/CompanyEdit";
import { CompanyList } from "./screens/company/CompanyList";

export const App = () => {
  const { authProvider, loading } = useAuthProvider();

  if (loading) {
    return (
      <Loading
        loadingPrimary="Loading"
        loadingSecondary="The page is loading, just a moment please"
      />
    );
  }

  return (
    <AdminContext
      dataProvider={dataProvider}
      authProvider={authProvider}
      i18nProvider={i18nProvider}
    >
      <DevSupport ComponentPreviews={ComponentPreviews} useInitialHook={useInitial}>
        <AdminUI layout={AdminLayout}>
          <Resource
            name="Company"
            options={{ label: "Company" }}
            list={CompanyList}
            recordRepresentation={getCompanyRecordRepresentation}
            create={CompanyCreate}
            edit={CompanyEdit}
          />
        </AdminUI>
      </DevSupport>
    </AdminContext>
  );
};
