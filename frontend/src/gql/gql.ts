/* eslint-disable */
import * as types from "./graphql";
import { TypedDocumentNode as DocumentNode } from "@graphql-typed-document-node/core";

/**
 * Map of all GraphQL operations in the project.
 *
 * This map has several performance disadvantages:
 * 1. It is not tree-shakeable, so it will include all operations in the project.
 * 2. It is not minifiable, so the string of a GraphQL query will be multiple times inside the bundle.
 * 3. It does not support dead code elimination, so it will add unused operations.
 *
 * Therefore it is highly recommended to use the babel or swc plugin for production.
 */
const documents = {
  "mutation UpdateCompany($input: CompanyInput!) {\n  updateCompany(input: $input) {\n    id\n    name\n    number\n    type\n  }\n}":
    types.UpdateCompanyDocument,
  "query Company($id: ID!) {\n  company(id: $id) {\n    id\n    name\n    number\n    type\n  }\n}":
    types.CompanyDocument,
  "query CompanyList {\n  companyList {\n    id\n    name\n    number\n    type\n  }\n}":
    types.CompanyListDocument,
  "mutation DeleteCompany($id: ID!) {\n  deleteCompany(id: $id) \n}":
    types.DeleteCompanyDocument,
  "\n     query userPermissions {\n         userPermissions\n     }\n":
    types.UserPermissionsDocument,
};

/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 *
 *
 * @example
 * ```ts
 * const query = gql(`query GetUser($id: ID!) { user(id: $id) { name } }`);
 * ```
 *
 * The query argument is unknown!
 * Please regenerate the types.
 */
export function gql(source: string): unknown;

/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(
  source: "mutation UpdateCompany($input: CompanyInput!) {\n  updateCompany(input: $input) {\n    id\n    name\n    number\n    type\n  }\n}"
): (typeof documents)["mutation UpdateCompany($input: CompanyInput!) {\n  updateCompany(input: $input) {\n    id\n    name\n    number\n    type\n  }\n}"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(
  source: "query Company($id: ID!) {\n  company(id: $id) {\n    id\n    name\n    number\n    type\n  }\n}"
): (typeof documents)["query Company($id: ID!) {\n  company(id: $id) {\n    id\n    name\n    number\n    type\n  }\n}"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(
  source: "query CompanyList {\n  companyList {\n    id\n    name\n    number\n    type\n  }\n}"
): (typeof documents)["query CompanyList {\n  companyList {\n    id\n    name\n    number\n    type\n  }\n}"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(
  source: "mutation DeleteCompany($id: ID!) {\n  deleteCompany(id: $id) \n}"
): (typeof documents)["mutation DeleteCompany($id: ID!) {\n  deleteCompany(id: $id) \n}"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(
  source: "\n     query userPermissions {\n         userPermissions\n     }\n"
): (typeof documents)["\n     query userPermissions {\n         userPermissions\n     }\n"];

export function gql(source: string) {
  return (documents as any)[source] ?? {};
}

export type DocumentType<TDocumentNode extends DocumentNode<any, any>> =
  TDocumentNode extends DocumentNode<infer TType, any> ? TType : never;
