package com.company.amplicodecompanyapp.graphql;

import com.amplicode.core.graphql.annotation.GraphQLId;
import com.company.amplicodecompanyapp.company.Company;
import com.company.amplicodecompanyapp.company.CompanyRepository;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Controller
public class CompanyController {
    private final CompanyRepository crudRepository;

    public CompanyController(CompanyRepository crudRepository) {
        this.crudRepository = crudRepository;
    }

    @MutationMapping(name = "deleteCompany")
    @Transactional
    public void delete(@GraphQLId @Argument @NonNull Long id) {
        Company entity = crudRepository.findById(id)
                .orElseThrow(() -> new RuntimeException(String.format("Unable to find entity by id: %s ", id)));

        crudRepository.delete(entity);
    }

    @QueryMapping(name = "companyList")
    @Transactional(readOnly = true)
    @NonNull
    public List<Company> findAll() {
        return crudRepository.findAll();
    }

    @QueryMapping(name = "company")
    @Transactional(readOnly = true)
    @NonNull
    public Company findById(@GraphQLId @Argument @NonNull Long id) {
        return crudRepository.findById(id)
                .orElseThrow(() -> new RuntimeException(String.format("Unable to find entity by id: %s ", id)));
    }

    @MutationMapping(name = "updateCompany")
    @Transactional
    @NonNull
    public Company update(@Argument @NonNull Company input) {
        if (input.getId() != null) {
            if (!crudRepository.existsById(input.getId())) {
                throw new RuntimeException(
                        String.format("Unable to find entity by id: %s ", input.getId()));
            }
        }
        return crudRepository.save(input);
    }
}