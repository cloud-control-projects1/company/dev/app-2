package com.company.amplicodecompanyapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

@SpringBootApplication
@ConfigurationPropertiesScan
public class AmplicodeCompanyAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(AmplicodeCompanyAppApplication.class, args);
    }
}
